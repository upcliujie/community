# UKUI兴趣小组（SIG）

UKUI(Ultimate Kylin User Interface) SIG小组致力于桌面环境相关软件包的规划、维护和升级工作


## 工作目标

- 负责 UKUI 4 相关软件包的规划、维护和升级
- 及时响应用户反馈，解决相关问题

## 邮件列表
- [ukui@lists.openkylin.top](mailto:ukui@lists.openkylin.top)

## repository

- [ukui-session-manager](https://gitee.com/openkylin/ukui-session-manager)
- [ukui-settings-daemon](https://gitee.com/openkylin/ukui-settings-daemon)
- [ukui-shell-integration](https://gitee.com/openkylin/ukui-shell-integration)
- [ukui-search](https://gitee.com/openkylin/ukui-search)
- [ukui-biometric-auth](https://gitee.com/openkylin/ukui-biometric-auth)
- [ukui-biometric-manager](https://gitee.com/openkylin/ukui-biometric-manager)
- [ukui-interface](https://gitee.com/openkylin/ukui-interface)
- [ukui-control-center](https://gitee.com/openkylin/ukui-control-center)
- [ukui-desktop-environment](https://gitee.com/openkylin/ukui-desktop-environment)
- [ukui-framework](https://gitee.com/openkylin/ukui-framework)
- [ukui-themes](https://gitee.com/openkylin/ukui-themes)
- [ukui-wallpapers](https://gitee.com/openkylin/ukui-wallpapers)
- [ukui-greeter](https://gitee.com/openkylin/ukui-greeter)
- [ukui-menu](https://gitee.com/openkylin/ukui-menu)
- [ukui-panel](https://gitee.com/openkylin/ukui-panel)
- [ukui-power-manager](https://gitee.com/openkylin/ukui-power-manager)
- [ukui-screensaver](https://gitee.com/openkylin/ukui-screensaver)
- [ukui-sidebar](https://gitee.com/openkylin/ukui-sidebar)
- [ukui-window-switch](https://gitee.com/openkylin/ukui-window-switch)
- [ukui-kwin](https://gitee.com/openkylin/ukui-kwin)
- [ukui-kwin-effects](https://gitee.com/openkylin/ukui-kwin-effects)
- [ukui-bluetooth](https://gitee.com/openkylin/ukui-bluetooth)
- [ukui-system-monitor](https://gitee.com/openkylin/ukui-system-monitor)
- [ukui-notification-daemon](https://gitee.com/openkylin/ukui-notification-daemon)
- [ukui-clock](https://gitee.com/openkylin/ukui-clock)
- [ukui-globaltheme](https://gitee.com/openkylin/ukui-globaltheme)
- [ukui-touch-settings-plugin](https://gitee.com/openkylin/ukui-touch-settings-plugin)
- [ukui-input-gather](https://gitee.com/openkylin/ukui-input-gather)
- [ukui-app-widget](https://gitee.com/openkylin/ukui-app-widget)
- [ukui-system-appwidget](https://gitee.com/openkylin/ukui-system-appwidget)
- [ubuntukylin-default-settings](https://gitee.com/openkylin/ubuntukylin-default-settings)
- [ubuntukylin-theme](https://gitee.com/openkylin/ubuntukylin-theme)
- [ubuntukylin-wallpapers](https://gitee.com/openkylin/ubuntukylin-wallpapers)
- [kylin-tablet-desktop-general](https://gitee.com/openkylin/kylin-tablet-desktop-general)
- [kylin-app-manager](https://gitee.com/openkylin/kylin-app-manager)
- [kylin-app-cgroupd](https://gitee.com/openkylin/kylin-app-cgroupd)
- [kylin-status-manager](https://gitee.com/openkylin/kylin-status-manager)
- [kylin-nm](https://gitee.com/openkylin/kylin-nm)
- [kylin-usb-creator](https://gitee.com/openkylin/kylin-usb-creator)
- [kylin-device-daemon](https://gitee.com/openkylin/kylin-device-daemon)
- [kylin-user-guide](https://gitee.com/openkylin/kylin-user-guide)
- [peony](https://gitee.com/openkylin/peony)
- [peony-extensions](https://gitee.com/openkylin/peony-extensions)
- [youker-assistant](https://gitee.com/openkylin/youker-assistant)
- [libpwquality](https://gitee.com/openkylin/libpwquality)
- [qt5-gesture-extensions](https://gitee.com/openkylin/qt5-gesture-extensions)
- [qt5-ukui-platformtheme](https://gitee.com/openkylin/qt5-ukui-platformtheme)
- [biometric-authentication](https://gitee.com/openkylin/biometric-authentication)
- [kwin](https://gitee.com/openkylin/kwin)
- [dmz-cursor-theme](https://gitee.com/openkylin/dmz-cursor-theme)
- [openkylin-theme](https://gitee.com/openkylin/openkylin-theme)
- [chinese-segmentation](https://gitee.com/openkylin/chinese-segmentation)
- [time-shutdown](https://gitee.com/openkylin/time-shutdown)
- [libkysset](https://gitee.com/openkylin/libkysset)
## SIG成员

### Owner

- [wx-kylin](https://gitee.com/wx-kylin)
- [瓶子](https://gitee.com/ccpz)

### Maintainers
- [larue](https://gitee.com/larue)
- [MouseZhang](https://gitee.com/MouseZhang)
- [kwwzzz](https://gitee.com/kwwzzz)
- [handsome_feng](https://gitee.com/handsome_feng)
- [iaom](https://gitee.com/iaom)
- [yangxibowen](https://gitee.com/yangxibowen)
- [ll-eleven](https://gitee.com/ll-eleven)
- [seolaeo_self](https://gitee.com/seolaeo_self)
- [zhaoshixu](https://gitee.com/zhaoshixu)
- [szm-min](https://gitee.com/szm-min)
- [hantengc](https://gitee.com/hantengc)
- [pang_yi](https://gitee.com/pang_yi)
- [flywm](https://gitee.com/flywm)
- [duchangfeng](https://gitee.com/duchangfeng)
- [zhao-kexin111](https://gitee.com/zhao-kexin111)
- [zhaikangning](https://gitee.com/zhaikangning)

### Committers

- [lisaifei](https://gitee.com/waltermitty121906)
