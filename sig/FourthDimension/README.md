## FourthDimension

FourthDimension（第四维度）由华中科技大学人工智能与嵌入式实验室联合言图科技研发的一款基于大语言模型的智能检索增强生成（RAG）系统。

## 工作目标

提供私域知识库的构建与生成式问答等功能。

## SIG成员

lswmaker(`liaoshuwei@yantu-ai.com`)

Memory455(`hujunjie@yantu-ai.com`)

## SIG邮件列表
fourthdimension@lists.openkylin.top
