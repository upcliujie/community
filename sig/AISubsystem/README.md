# AISubsystem兴趣小组（SIG）

AISubsystem SIG小组致力于OpenKylin AI子系统的规划、维护和升级工作

## 工作目标

- 负责AI子系统相关软件包的规划、维护和升级
- 管理本地和云端大模型，屏蔽各个大模型和框架的差异，提供AI基础能力和特定场景相关的SDK接口
- 打造麒麟 OS Agent

## 维护包列表

- [kylin-ai-sdk](https://gitee.com/openkylin/kylin-ai-sdk)
- [kylin-ai-runtime](https://gitee.com/openkylin/kylin-ai-runtime)
- [kylin-ai-engine](https://gitee.com/openkylin/kylin-ai-engine)
- [kylin-ai-proto](https://gitee.com/openkylin/kylin-ai-proto)
- [kylin-baidu-ai-engine-plugin](https://gitee.com/openkylin/kylin-baidu-ai-engine-plugin)
- [kylin-baidu-nlp-engine](https://gitee.com/openkylin/kylin-baidu-nlp-engine)
- [kylin-baidu-speech-engine](https://gitee.com/openkylin/kylin-baidu-speech-engine)
- [kylin-baidu-vision-engine](https://gitee.com/openkylin/kylin-baidu-vision-engine)
- [kylin-ai-model-manager](https://gitee.com/openkylin/kylin-ai-model-manager)
- [kylin-model-management-tools](https://gitee.com/openkylin/kylin-model-management-tools)
- [kylin-ondevice-ai-engine-plugin](https://gitee.com/openkylin/kylin-ondevice-ai-engine-plugin)
- [kylin-ondevice-speech-engine](https://gitee.com/openkylin/kylin-ondevice-speech-engine)
- [kylin-ondevice-nlp-engine](https://gitee.com/openkylin/kylin-ondevice-nlp-engine)
- [kylin-xunfei-ai-engine-plugin](https://gitee.com/openkylin/kylin-xunfei-ai-engine-plugin)
- [kylin-xunfei-speech-engine](https://gitee.com/openkylin/kylin-xunfei-speech-engine)

## SIG 成员

### Owner
- [ningsiguang](https://gitee.com/ningsiguang)
- [flywm](https://gitee.com/flywm)
- [pang_yi](https://gitee.com/pang_yi)
- [amphetaminewei](https://gitee.com/amphetaminewei)
- [瓶子](https://gitee.com/ccpz)
### Maintainers
- [wangyan828](https://gitee.com/wangyan828)
- [zhao-kexin111](https://gitee.com/zhao-kexin111)
- [kylin-zexy](https://gitee.com/kylin-zexy)
- [akajd](https://gitee.com/akajd)
- [kingysu](https://gitee.com/kingysu)
- [linyuxuanlinkun](https://gitee.com/linyuxuanlinkun)
- [hantengc](https://gitee.com/hantengc)

## 邮件列表

- [aisubsystem@lists.openkylin.top](mailto:aisubsystem@lists.openkylin.top)
