# kaiming remote-add 添加软件源仓库

> 默认安装 ``kaiming`` deb包后就自动添加了 ``kaiming-repo`` 仓库，可以通过 ``kaiming remotes -d`` 进行查看已添加的软件源仓库。

## kaiming-repo 仓库已有软件列表

以下是列出的部分  ``kaiming-repo`` 已有软件列表，可参考 [kaiming install](./kaiming-install.md) 进行安装：
- 开源应用

    org.kde.okular

    org.kde.kcalc

    org.kde.kweather

    org.kde.kclock

    org.kde.isoimagewriter

    org.kde.haruna

    org.kde.kamoso

    org.kde.elisa

    org.kde.konsole

    org.kde.ark

    org.kde.filelight

    org.kde.krdc

    org.kde.kigo

    org.kde.kapman

    org.kde.kolourpaint

    org.kde.kronometer

    org.kde.itinerary

    org.kde.palapeli



- 自研应用：

    top.openkylin.Clock

    top.openkylin.PhotoViewer


- 依赖包

    top.openkylin.Platform

    org.freedesktop.Platform.GL.default

    org.freedesktop.Platform.VAAPI.Intel

    org.kde.Sdk.Docs

