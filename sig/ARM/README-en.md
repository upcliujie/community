## ARM SIG

ARM SIG is committed to the co-construction and promotion of the ARM architecture ecosystem, as well as supporting the ARM Vendors platform based on the openKylin kernel.

## Goal-setting

1. Accelerate the resolution of common Arm architecture issues in the upstream community, or backport features from the upstream community to solve openKylin Kernel issues.
2. Collect Arm architecture user and manufacturer requirements and improve the openKylin Kernel Arm ecosystem.
3. Joint sharing and cooperative development and evolution of related migration tools and optimization tools.

## SIG Member

## Maintainers
gookevin2019 (`gujian@phytium.com.cn`)
mao-hongbo (`maohongbo@phytium.com.cn`)
shuaijiakun (`shuaijiakun1288@phytium.com.cn`)
mr-liubaggio (`liuyh62@lenovo.com`)

## SIG Maintenance Packages List

## Mailing List
arm@lists.openkylin.top
