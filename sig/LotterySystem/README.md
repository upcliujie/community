# LotterySystem兴趣小组（SIG）

LotterySystem SIG小组是由深圳穗彩科技成立的面向彩票行业的解决方案，致力于推动彩票行业操作系统的国产化，构建安全可靠的彩票行业系统环境。

## 工作目标

- 基于openKylin系统适配彩票行业相关软硬件设设备，拓展openKylin软硬件生态；

- 基于openKylin构建面向彩票行业安全好用的衍生发行版


## SIG 成员

### Owner
- heze bingze.he@genlot.com

### Maintainers
- guoyong yong.guo@genlot.com

## 维护包列表


## 邮件列表

- [lotterysystem@lists.openkylin.top](mailto:lotterysystem@lists.openkylin.top)
