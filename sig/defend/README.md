# 防护系统 SIG

主要讨论在openKylin社区版本中已有或未来可能新增或外部支持可引入的系统防护技术：

- 讨论现有的基于平台、体系结构等的系统防护技术
- 讨论现阶段主流的恶意软件防护、流行的恶意入侵保护等相关技术，并且将其实例化
- 在openKylin社区版本中针对特定、已有、可实施的系统防护技术提供系统防护工具、库、基础组件以及外部检测接口等基础设施，基于系统原生支持提升系统整体安全性
- 针对系统防护做后续升级，针对当前系统做整体保护
- 根据openKylin未来的安全技术规划，结合现有的各种技术原理，做未来系统的防护软件的更新、方向的指定、目标的规划

## 仓库
- defend-management
- all

## SIG成员
### Owner
- sqlxss(赵建祖)

### Maintainers
- sqlxss
- Kernal


## 邮件列表
defend@lists.openkylin.top
<br>
zhaojianzu@qq.com