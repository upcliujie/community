
## 操作系统领域知识图谱

KG4OS SIG(知识图谱SIG组)致力于从多维度、细粒度建立操作系统领域知识图谱

## 工作内容

1、分析操作系统软件包元数据、二进制符号表等解析软件包依赖关系机制；

2、构建openKylin操作系统软件包知识图谱，包括供应链关系、软件包依赖关系、冲突关系等；

3、基于构建的知识图谱分析openKylin操作系统演化过程中出现的兼容性故障机理，研究演化过程中依赖关系治理机制、兼容型保障策略和机制。

## SIG成员

SIG-owner(`majun@ubuntukylin.com`,`ddjoyce115@vip.163.com`,`zxyssy28@163.com`)

## SIG邮件列表
kg4os@lists.openkylin.top

## SIG组例会
双周例会