## 系统管家SIG

### 工作目标
本SIG组致力于组建系统管家开源社区，负责开发和维护系统管家及附属工具，为openkylin生态和实用性添砖加瓦，欢迎各位的加入！

### SIG成员
- liuyuhui@kylinos.cn
- jishengjie@kylinos.cn
- zhangxinxin_1@kylinos.cn
- fanyuchen@kylinos.cn

### SIG维护包列表
- kylin-os-manager

