# LoongArch SIG

负责LoongArch开源软件包的维护。发布openKylin的LoongArch版本，进行软件包构建、系统构建等工作。

## 工作目标

- 负责 openKylin LoongArch 版本的规划、制作、维护和升级

## 邮件列表

loongarch@lists.openkylin.top

## SIG成员

### Owner

- [lsnwhb](https://gitee.com/lsnwhb)
- [isangmeng](https://gitee.com/isangmeng)

### Maintainers

- wanghonghu@loongson.cn

### Committers

## 仓库列表
