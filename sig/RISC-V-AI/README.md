# RISC-V-AI SIG

致力于RISC-V 与 AI 技术的融合，提高 openKylin 操作系统及AI应用在RISC-V架构下的性能与可移植性。

## 工作目标

- 负责 openKylin RISC-V AI推理框架的优化、维护和升级;
- 探索 AI 在 RISC-V 平台上的优化技术，包括算子库、内存和任务调度等。

## 邮件列表

risc-v@lists.openkylin.top

## SIG成员

### Owner

- [wenzhuw](https://gitee.com/wenzhuw)


### Maintainers

- li_ke@hl-it.cn
- liulili1010@hl-it.cn
- wangyunlong1118@163.com



## 仓库列表
- [kylin-riscv-package](https://gitee.com/openkylin/kylin-riscv-package)
- [kylin-riscv-paper](https://gitee.com/openkylin/kylin-riscv-paper)