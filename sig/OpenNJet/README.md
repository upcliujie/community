## OpenNJet SIG

主要在于云原生应用引擎的探索与建设，提高open Kylin操作系统适配下应用的高可用性，让客户的应用永远在线！

## 工作目标：

本SIG组负责基于open Kylin系统的OpenNJet软件包的规划、维护和升级工作。

## SIG成员：
- 李敏  limins@tmlake.com

## 邮件列表：
mailman@njet.org.cn

