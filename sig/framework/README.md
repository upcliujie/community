# Framework兴趣小组（SIG）

Framework SIG致力于为openKylin社区提供集程序编辑、编译、调试、发布、分析等全套开发功能的编程环境，涵盖通用集成开发环境、编译工具链、运行时环境、类库等。SIG初期重点研制高效率、跨平台、插件化、易调试的通用集成开发环境，支持C、C++、Java、Go、Python、JavaScript等多种标准编程语言，涵盖编码、编译、调试、性能分析、软件交付等一整套开发流程，满足openKylin平台上软件开发需求。


## 工作目标

- 研制openKylin集成开发环境，包含程序编辑、编译、调试、发布、分析等开发功能。
- 研制基于openKylin平台的编译工具，高级语言运行时环境等组件。


## repository
- [extensions-repo](https://gitee.com/openkylin/extensions-repo) IDE汇总
- [kylin-code](https://gitee.com/openkylin/kylin-code) IDE基础平台
- [ide-extension-dependency](https://gitee.com/openkylin/ide-extension-dependency) 插件依赖管理插件
- [ide-offline-extensions-manager](https://gitee.com/openkylin/ide-offline-extensions-manager) 离线插件管理插件
- [vscode-create-project](https://gitee.com/openkylin/vscode-create-project) 项目创建和打包插件
- [ide-gitlens](https://gitee.com/openkylin/ide-gitlens) Gitlens插件，Git增强
- [cmake-intellisence](https://gitee.com/openkylin/cmake-intellisence) Cmake编辑支持插件
- [ide-vscode-clangd](https://gitee.com/openkylin/ide-vscode-clangd) C/C++语言编辑支持插件
- [native-debug](https://gitee.com/openkylin/ide-vscode-clangd) C/C++语言调试(GDB)支持插件
- [ide-vscode-java-pack](https://gitee.com/openkylin/ide-vscode-java-pack) Java总包插件，安装该插件会将Java语言支持类插件都下载安装
- [ide-java](https://gitee.com/openkylin/ide-java) Java语言编辑支持插件
- [vscode-java-debug](https://gitee.com/openkylin/vscode-java-debug) Java语言调试支持插件
- [ide-vscode-java-dependency](https://gitee.com/openkylin/ide-vscode-java-dependency) Java语言项目支持相关插件
- [ide-vscode-maven](https://gitee.com/openkylin/ide-vscode-maven) Maven支持插件
- [ide-vscode-gradle](https://gitee.com/openkylin/ide-vscode-gradle) Gradle支持插件
- [ide-vscode-java-test](https://gitee.com/openkylin/ide-vscode-java-test) Java代码测试支持插件
- [ide-python](https://gitee.com/openkylin/ide-python) python语言编辑、调试支持插件
- [ide-go](https://gitee.com/openkylin/ide-go) Go语言编辑、调试支持插件
- [ide-js-debug](https://gitee.com/openkylin/ide-js-debug) JS调试支持插件
- [deadlock-detect](https://gitee.com/openkylin/deadlock-detect) 死锁检测插件
- [ide-memleak-detect](https://gitee.com/openkylin/ide-memleak-detect) 内存泄漏检测插件


## SIG成员
### Maintainers
- wuchunguang(wuchunguang@kylinos.cn)
- liangkeming(liangkeming@kylinos.cn)
- mu_ruichao(muruichao@kylinos.cn)
- xw1985(xuewei@kylinos.cn)
- zhangyun-2022(zhangyun@kylinos.cn)
- chriswang521(wangpenglong@kylinos.cn)
- linchaochao(linchaochao@kylinos.cn)
- quanzhuo(quanzhuo@kylinos.cn)
- chenhaoyang2019(chenhaoyang@kylinos.cn)
- xhafei(xuhong@kylinos.cn)
- leven08(lijinwen@kylinos.cn)
- mcy-kylin(machunyu@kylinos.cn)
- blueskycs2c(dinglili@kylinos.cn)
- cxdgrck(chenxi@kylinos.cn)
- flowing-haze(xiayuqi@kylinos.cn)
- changtao615(changtao@kylinos.cn)
