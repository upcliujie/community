# SWArch SIG

参与SW ARCH开源软件包的维护，协助openKylin的SW架构版本的开发及维护。

## 工作目标

- 负责 openKylin SW ARCH 版本的规划、制作、维护和升级

## 邮件列表

swarch@lists.openkylin.top

## SIG成员

### Owner

- [wuzx](https://gitee.com/wuzx065891)

### Maintainers


### Committers


## 仓库列表

