# GPU (SIG)

openKylin GPU小组致力于GPU驱动相关技术研究，包括OpenGL、OpenCL、Vulkan、VDPAU和VAAPI等。提供GPU相关软件包的技术规划、设计、开发、维护和升级服务，共同推动国产GPU技术发展。

## 工作目标

- 负责国产GPU驱动规划、开发和推广
- 共同规划国产GPU驱动技术发展路线
- 共同推动GPU技术发展

## SIG成员

### Owner
- 单晋奎

### Maintainers
- 单晋奎
- 易亮亮
- 王蕙
- 赵杨
- 李国强
- [孔建军@Sietium](https://gitee.com/amoskong)
- [汪宇臻@InnoSilicon](https://gitee.com/wavestyle)

### Contributors
- 韩姣（han777j  hanj@innosilicon.com.cn）
- 陈肖（chen_sp chenxiao@innosilicon.com.cn）
- 罗勇（alexrowe120 luoy@innosilicon.com.cn）
- 杜希骏（duxijun duxj@innosilicon.com.cn）
- 张之龙（zhangzhilong zhangzhilong@idmgpu.com）

## SIG维护包列表
- libdrm
- mesa
- vulkan
- libvdpau
- llvm
- xorg-server

## 邮件列表
gpu@lists.openkylin.top
